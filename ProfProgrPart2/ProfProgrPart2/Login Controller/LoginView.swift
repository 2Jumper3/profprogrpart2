//
//  LoginView.swift
//  ProfProgrPart2
//
//  Created by Sergey on 13.04.2020.
//  Copyright © 2020 Sergey. All rights reserved.
//


import UIKit
import RxCocoa
import RxSwift

class LoginView: UIViewController {
    
    var viewModel: LoginViewModel?

    //MARK: - UI settings
    private(set) lazy var userNameTextField: ShakingAnimation = {
        let textField = ShakingAnimation()
        textField.translatesAutoresizingMaskIntoConstraints = false
        textField.font = UIFont.systemFont(ofSize: 10)
        textField.placeholder = "UserName (at least 3 symbols)"
        textField.spellCheckingType = .no
        textField.keyboardType = .default
        textField.backgroundColor = .white
        textField.textAlignment = .center
        textField.layer.cornerRadius = 10
        return textField
    }()
    
    private(set) lazy var passwordTextField: ShakingAnimation = {
        let textField = ShakingAnimation()
        textField.translatesAutoresizingMaskIntoConstraints = false
        textField.font = UIFont.systemFont(ofSize: 10)
        textField.placeholder = "Password (at least 3 symbols)"
        textField.isSecureTextEntry = true
        textField.autocorrectionType = .no
        textField.backgroundColor = .white
        textField.textAlignment = .center
        textField.layer.cornerRadius = 10
        return textField
    }()
    
    private(set) lazy var  loginBtn: UIButton =  {
        let btn = UIButton()
        btn.translatesAutoresizingMaskIntoConstraints = false
        btn.setTitle("Login", for: .normal)
        btn.tintColor = .black
        btn.setTitle("Fill all Fields", for: .disabled)
        btn.setBackgroundColor(color: .gray, forState: .disabled)
        btn.addTarget(self, action: #selector(btnTapped), for: .touchUpInside)
        btn.backgroundColor = .darkGray
        return btn
    }()
    
    private(set) lazy var  registerBtn: UIButton =  {
        let btn = UIButton()
        btn.translatesAutoresizingMaskIntoConstraints = false
        btn.setTitle("Register", for: .normal)
        btn.addTarget(self, action: #selector(openRegisterVC), for: .touchUpInside)
        btn.tintColor = .black
        btn.backgroundColor = .darkGray
        return btn
    }()
    
    override func viewDidLoad() {
        self.viewModel = LoginViewModel(login: "", password: "")
        navigationControllerSetup()
        setupLayout()
        activateLoginbtn() 
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = false
        self.userNameTextField.text = ""
        self.passwordTextField.text = ""
    }
    
    //MARK: - NavigationController settings
    
    func navigationControllerSetup() {
        self.navigationController?.navigationBar.isHidden = false
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.navigationBar.tintColor = .black
    }

    //MARK: - Constrains and subview

    private func setupLayout() {
        self.view.addSubview(userNameTextField)
        self.view.addSubview(passwordTextField)
        self.view.addSubview(loginBtn)
        self.view.addSubview(registerBtn)
        self.view.backgroundColor = .lightGray
        
        let topAnchorContraint: CGFloat = 200
        let widthAnchorContraint: CGFloat = 250
        let spacingContraint: CGFloat = 15
        let heightAnchorContraint: CGFloat = 30
        
        
        NSLayoutConstraint.activate([
            self.userNameTextField.topAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.topAnchor, constant: topAnchorContraint),
            self.userNameTextField.widthAnchor.constraint(equalToConstant: widthAnchorContraint),
            self.userNameTextField.centerXAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.centerXAnchor),
            self.userNameTextField.heightAnchor.constraint(equalToConstant: heightAnchorContraint),
            
            self.passwordTextField.topAnchor.constraint(equalTo: self.userNameTextField.bottomAnchor, constant: spacingContraint),
            self.passwordTextField.widthAnchor.constraint(equalToConstant: widthAnchorContraint),
            self.passwordTextField.centerXAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.centerXAnchor),
            self.passwordTextField.heightAnchor.constraint(equalToConstant: heightAnchorContraint),
            
            self.loginBtn.topAnchor.constraint(equalTo: self.passwordTextField.bottomAnchor, constant: spacingContraint),
            self.loginBtn.widthAnchor.constraint(equalToConstant: widthAnchorContraint),
            self.loginBtn.centerXAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.centerXAnchor),
            self.loginBtn.heightAnchor.constraint(equalToConstant: heightAnchorContraint),
            
            self.registerBtn.topAnchor.constraint(equalTo: self.loginBtn.bottomAnchor, constant: spacingContraint),
            self.registerBtn.widthAnchor.constraint(equalToConstant: widthAnchorContraint),
            self.registerBtn.centerXAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.centerXAnchor),
            self.registerBtn.heightAnchor.constraint(equalToConstant: heightAnchorContraint),

        ])
    }
    
    //MARK:- Actions
    
    @objc func btnTapped() {
        do {
            let valid = try viewModel?.login(userName: userNameTextField, passWord: passwordTextField)
            if valid! {
                let vc = ViewController()
                self.navigationController?.pushViewController(vc, animated: true)

            } else  {
                let alert = UIAlertController(title: "Wrong Login or Password", message: "", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "Done", style: .cancel, handler: nil))
                self.present(alert, animated: true, completion: nil)
            }
        } catch { print("error")}
    }
    
    @objc func openRegisterVC() {
        let vc = RegisterView()
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func activateLoginbtn() {
        Observable
            .combineLatest(userNameTextField.rx.text, passwordTextField.rx.text)
            .map { login, password in
                return !(login ?? "").isEmpty && (password ?? "").count >= 3
            }
            .bind { [weak loginBtn] inputFilled in
                UIView.transition(with: loginBtn!,
                                  duration: 0.5,
                                  options: .transitionCrossDissolve,
                                  animations: { loginBtn!.isEnabled = true },
                                  completion: nil)
            loginBtn?.isEnabled = inputFilled
        }
    }
    
    deinit {
        print ("loginVC Deinited")
    }
    
}
