//
//  LoginViewModel.swift
//  ProfProgrPart2
//
//  Created by Sergey on 13.04.2020.
//  Copyright © 2020 Sergey. All rights reserved.
//

import Foundation
import UIKit
import RealmSwift
import RxSwift
import RxCocoa

class LoginViewModel: UIViewController {
    
    var model: User?
    var dbmanager:DBManager = DBManagerImpl()
    
    init(login: String, password: String) {
        self.model?.login = login
        self.model?.password = password
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    

    
    func login(userName: ShakingAnimation, passWord: ShakingAnimation) throws -> Bool  {
        
        guard let username = userName.text, !username.isEmpty else {throw CustomError.error1(func: userName.shake())}
        guard let password = passWord.text, !password.isEmpty else {throw CustomError.error1(func: passWord.shake())}
        var condition = true
        let users = dbmanager.getUser()

        for index in users  {
            if index.login == username && index.password == password {
                print("success")
                condition = true
                break
            } else  {
                print("u r wrong")
                condition = false
            }
        }
        return condition
    }
}
