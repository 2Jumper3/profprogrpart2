//
//  ViewController.swift
//  ProfProgrPart2
//
//  Created by Sergey on 31.03.2020.
//  Copyright © 2020 Sergey. All rights reserved.
//

import UIKit
import GoogleMaps
import CoreLocation
import RealmSwift
import RxSwift
import RxCocoa

class ViewController: UIViewController {
    
    var locationManager = LocationManager.instance
    var startStopBtn: UIButton! // Start/ Stop tracking btn
    let realm = try! Realm()
    let dbManager: DBManager = DBManagerImpl() // Data Base Manager
    var status: Bool? // Текущий статус
    let notification = Notifications()
    
    var allPoints:[CLLocationCoordinate2D]=[] //Временный контейнер для сохранения в реалм
    var routePath = GMSMutablePath()  //gmsPath
    var route: GMSPolyline?
    var marker = GMSMarker()
    var mapView = GMSMapView()

    
    let coordinate = CLLocationCoordinate2D(latitude: 55.75578600, longitude: 37.61763300)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Tracking"
        configureLocationManager()
        addMapView()
        addButtons()
    }
    override func viewDidAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = true

    }
//MARK: - Config Location Manager
    
        func configureLocationManager() {
            locationManager
                .location
                .asObservable()
                .bind { [weak self] location in
                    guard let location = location else { return }
                    self?.routePath.add(location.coordinate)
                    self?.route?.path = self?.routePath
                    let position = GMSCameraPosition.camera(withTarget: location.coordinate, zoom: 17)
                    self?.addMarker(coordinate: location.coordinate)
                    self?.mapView.animate(to: position)
            }
        }
    

    func addMapView() {
        let camera = GMSCameraPosition.camera(withTarget: coordinate, zoom: 10.0)
        self.mapView = GMSMapView.map(withFrame: self.view.frame, camera: camera)
        self.view.addSubview(self.mapView)
        
        guard let image = loadImageFromDiskWith(fileName: "imageName") else {return}
        let resizedImage = resizeImage(image: image, targetSize: CGSize(width: 40.0, height: 40.0))
        let imageView = UIImageView(image: resizedImage)
        imageView.clipsToBounds = true
        imageView.layer.cornerRadius = 20
        self.marker.iconView = imageView
        
    }
    //MARK: - Настройка кнопок для управления
    func addButtons() {
        self.startStopBtn = {
            let btn = UIButton()
            btn.translatesAutoresizingMaskIntoConstraints = false
            btn.setTitle("Start", for: .normal)
            btn.layer.cornerRadius = 10
            btn.addTarget(self, action: #selector(startUpdatingLocation), for: .touchUpInside)
            btn.backgroundColor = .lightGray
            btn.setTitleColor(.white, for: .normal)
            return btn
        }()
        
        let saveLastSession: UIButton = {
            let btn = UIButton()
            btn.translatesAutoresizingMaskIntoConstraints = false
            btn.setTitle("Save", for: .normal)
            btn.layer.cornerRadius = 10
            btn.addTarget(self, action: #selector(saveLastWay), for: .touchUpInside)
            btn.backgroundColor = .lightGray
            btn.setTitleColor(.white, for: .normal)
            return btn
        }()
        
        let loadLastSession: UIButton = {
            let btn = UIButton()
            btn.translatesAutoresizingMaskIntoConstraints = false
            btn.setTitle("Load", for: .normal)
            btn.layer.cornerRadius = 10
            btn.addTarget(self, action: #selector(loadLastWay), for: .touchUpInside)
            btn.backgroundColor = .lightGray
            btn.setTitleColor(.white, for: .normal)
            return btn
        }()
        
        let logOut: UIButton = {
            let btn = UIButton()
            btn.translatesAutoresizingMaskIntoConstraints = false
            btn.setTitle("Log Out", for: .normal)
            btn.layer.cornerRadius = 10
            btn.addTarget(self, action: #selector(logOutFunc), for: .touchUpInside)
            btn.backgroundColor = .lightGray
            btn.setTitleColor(.white, for: .normal)
            return btn
        }()
        
        let settingsBtn: UIButton = {
            let btn = UIButton()
            btn.translatesAutoresizingMaskIntoConstraints = false
            btn.setTitle("Settings", for: .normal)
            btn.layer.cornerRadius = 10
            btn.addTarget(self, action: #selector(goToSettings), for: .touchUpInside)
            btn.backgroundColor = .lightGray
            btn.setTitleColor(.white, for: .normal)
            return btn
        }()
    
        self.view.addSubview(startStopBtn)
        self.view.addSubview(saveLastSession)
        self.view.addSubview(loadLastSession)
        self.view.addSubview(logOut)
        self.view.addSubview(settingsBtn)
        let topAnchorConstant: CGFloat = -130
        let spacing: CGFloat = 20

        NSLayoutConstraint.activate([
            startStopBtn.topAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.bottomAnchor, constant: topAnchorConstant),
            startStopBtn.leftAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.leftAnchor),
            startStopBtn.widthAnchor.constraint(equalToConstant: self.view.frame.size.width / 2),
            startStopBtn.bottomAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.bottomAnchor, constant: -100),
            
            saveLastSession.topAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.bottomAnchor, constant: topAnchorConstant),
            saveLastSession.rightAnchor.constraint(equalTo: loadLastSession.leftAnchor),
            saveLastSession.widthAnchor.constraint(equalToConstant: self.view.frame.size.width / 4),
            saveLastSession.bottomAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.bottomAnchor, constant: -100),
            
            loadLastSession.topAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.bottomAnchor, constant: topAnchorConstant),
            loadLastSession.rightAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.rightAnchor),
            loadLastSession.widthAnchor.constraint(equalToConstant: self.view.frame.size.width / 4),
            loadLastSession.bottomAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.bottomAnchor, constant: -100),
            
            settingsBtn.topAnchor.constraint(equalTo: self.startStopBtn.bottomAnchor, constant: spacing),
            settingsBtn.leftAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.leftAnchor),
            settingsBtn.widthAnchor.constraint(equalToConstant: self.view.frame.size.width / 2),
            settingsBtn.bottomAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.bottomAnchor, constant: -50),
            
            logOut.topAnchor.constraint(equalTo: self.startStopBtn.bottomAnchor, constant: spacing),
            logOut.rightAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.rightAnchor),
            logOut.widthAnchor.constraint(equalToConstant: self.view.frame.size.width / 2),
            logOut.bottomAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.bottomAnchor, constant: -50)
        ])
    }
    //MARK: - Методы для кнопок
    
    @objc func startUpdatingLocation() {
        if status == true {
            self.startStopBtn.setTitle("START", for: .normal)
            print("stopUpdatingLocation")
            locationManager.stopUpdatingLocation()
            status = false
        } else {
            self.startStopBtn.setTitle("STOP", for: .normal)
            print("startUpdatingLocation")
                    route?.map = nil
                    route = GMSPolyline()
            route?.strokeWidth = 3.0
                    routePath = GMSMutablePath()
                    route?.map = mapView
                    locationManager.startUpdatingLocation()
                    status = true
        }
    }
    
    
    @objc func saveLastWay() {
        print("Save")
        if status == true {
            let alert = UIAlertController(title: "Please Stop Tracking", message: "Press Stop Btn", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "ok", style: .cancel, handler: nil))
                self.present(alert, animated: true, completion: nil)
        } else {
            self.mapView.clear()
            saveToRealm()
        }
    }
    
    @objc func loadLastWay() {
        loadFromRealm()
    }
    
    @objc func logOutFunc()  {
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func goToSettings() {
        let vc = SettingViewController()
        self.navigationController?.pushViewController(vc, animated: true)
    }

   //MARK: - Сохранение/загрука в/из реалм(а)
    
    
    func saveToRealm() {
        var count = 0
        let list = List<NewRealmModel>()
        for index in self.allPoints {
            let realmPoints = NewRealmModel(lat: index.latitude, lng: index.longitude, timePerSec: count)
            list.append(realmPoints)
            count = count + 1
        }
        dbManager.save(coordinate: list)
        print("realmmm !!!!! \(realm.configuration.fileURL)")
    }
    
    func loadFromRealm() {
        let lastWay = dbManager.getCoordinates()
        route?.map = nil
        for index in lastWay {
            routePath.add(CLLocationCoordinate2D(latitude: index.latitude, longitude: index.longitude))
        }
        route?.path = routePath
        route?.map = mapView
        
        print("last way printed \(routePath.count())")
    }
    
    deinit {
        print ("mapVC Deinited")
    }
}


//MARK: - Extension/Draw
extension ViewController: CLLocationManagerDelegate {
    
    func addMarker(coordinate: CLLocationCoordinate2D) {
        self.marker.map = nil
        self.marker.position = coordinate
        self.marker.map = mapView
        
    }

    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print(error)
    }
    
    func loadImageFromDiskWith(fileName: String) -> UIImage? {

      let documentDirectory = FileManager.SearchPathDirectory.documentDirectory

        let userDomainMask = FileManager.SearchPathDomainMask.userDomainMask
        let paths = NSSearchPathForDirectoriesInDomains(documentDirectory, userDomainMask, true)

        if let dirPath = paths.first {
            let imageUrl = URL(fileURLWithPath: dirPath).appendingPathComponent(fileName)
            let image = UIImage(contentsOfFile: imageUrl.path)
            return image

        }

        return nil
    }
    
     func resizeImage(image: UIImage, targetSize: CGSize) -> UIImage {
        let size = image.size

        let widthRatio  = targetSize.width  / size.width
        let heightRatio = targetSize.height / size.height
        var newSize: CGSize
        if(widthRatio > heightRatio) {
            newSize = CGSize(width: size.width * heightRatio, height: size.height * heightRatio)
        } else {
            newSize = CGSize(width: size.width * widthRatio,  height: size.height * widthRatio)
        }
        let rect = CGRect(x: 0, y: 0, width: newSize.width, height: newSize.height)
        UIGraphicsBeginImageContextWithOptions(newSize, false, 1.0)
        image.draw(in: rect)
        
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()

        return newImage!
    }

}
