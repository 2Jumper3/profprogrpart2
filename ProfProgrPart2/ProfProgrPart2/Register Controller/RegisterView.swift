//
//  RegisterView.swift
//  ProfProgrPart2
//
//  Created by Sergey on 13.04.2020.
//  Copyright © 2020 Sergey. All rights reserved.
//

import Foundation
import UIKit
import RxSwift
import RxCocoa

class RegisterView: UIViewController {
    
    var viewModel: RegisterViewModel?
    
    //MARK: - UI settings
    private(set) lazy var userNameTextField: ShakingAnimation = {
        let textField = ShakingAnimation()
        textField.translatesAutoresizingMaskIntoConstraints = false
        textField.backgroundColor = .white
        textField.font = UIFont.systemFont(ofSize: 10)
        textField.placeholder = "UserName (at least 3 symbols)"
        textField.textAlignment = .center
        textField.layer.cornerRadius = 10
        return textField
    }()

    private(set) lazy var passwordTextField: ShakingAnimation = {
        let textField = ShakingAnimation()
        textField.translatesAutoresizingMaskIntoConstraints = false
        textField.font = UIFont.systemFont(ofSize: 10)
        textField.placeholder = "PassWord (at least 3 symbols)"
        textField.backgroundColor = .white
        textField.isSecureTextEntry = true
        textField.autocorrectionType = .no
        textField.textAlignment = .center
        textField.layer.cornerRadius = 10
        return textField
    }()
    
    
    private(set) lazy var  registerBtn: UIButton =  {
        let btn = UIButton()
        btn.translatesAutoresizingMaskIntoConstraints = false
        btn.setTitle("Register", for: .normal)
        btn.setTitle("Not Active", for: .disabled)
        btn.setBackgroundColor(color: .gray, forState: .disabled)
        btn.tintColor = .black
        btn.addTarget(self, action: #selector(btnTapped), for: .touchUpInside)
        btn.backgroundColor = .darkGray
        btn.isEnabled = true
        return btn
    }()

    
    override func viewDidLoad() {
        self.navigationController?.navigationBar.isHidden = false
        self.navigationController?.view.backgroundColor = .lightGray
        self.viewModel = RegisterViewModel(login: "", password: "")
        activateRegisterbtn() 
        setupLayout()
    }
    //MARK: - Constraints

    private func setupLayout() {
        self.view.addSubview(userNameTextField)
        self.view.addSubview(passwordTextField)
        self.view.addSubview(registerBtn)

        self.view.backgroundColor = .lightGray
        
        let topAnchorContraint: CGFloat = 200
        let widthAnchorContraint: CGFloat = 250
        let spacingContraint: CGFloat = 15
        let heightAnchorContraint: CGFloat = 30
        
        
        NSLayoutConstraint.activate([
            self.userNameTextField.topAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.topAnchor, constant: topAnchorContraint),
            self.userNameTextField.widthAnchor.constraint(equalToConstant: widthAnchorContraint),
            self.userNameTextField.centerXAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.centerXAnchor),
            self.userNameTextField.heightAnchor.constraint(equalToConstant: heightAnchorContraint),
            
            self.passwordTextField.topAnchor.constraint(equalTo: self.userNameTextField.bottomAnchor, constant: spacingContraint),
            self.passwordTextField.widthAnchor.constraint(equalToConstant: widthAnchorContraint),
            self.passwordTextField.centerXAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.centerXAnchor),
            self.passwordTextField.heightAnchor.constraint(equalToConstant: heightAnchorContraint),
            
            self.registerBtn.topAnchor.constraint(equalTo: self.passwordTextField.bottomAnchor, constant: spacingContraint),
            self.registerBtn.widthAnchor.constraint(equalToConstant: widthAnchorContraint),
            self.registerBtn.centerXAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.centerXAnchor),
            self.registerBtn.heightAnchor.constraint(equalToConstant: heightAnchorContraint),
            
        ])
    }
    
    
    func activateRegisterbtn() {
        Observable
            .combineLatest(userNameTextField.rx.text, passwordTextField.rx.text)
            .map { login, password in
                return !(login ?? "").isEmpty && (password ?? "").count >= 3
            }
            .bind { [weak registerBtn] inputFilled in
                UIView.transition(with: registerBtn!,
                                  duration: 0.5,
                                  options: .transitionCrossDissolve,
                                  animations: { registerBtn!.isEnabled = true },
                                  completion: nil)
            registerBtn?.isEnabled = inputFilled
        }
    }
    
    //MARK: - OBJ-C Methods
    
    @objc func btnTapped() {
        print("tapped")
        do {
            let valid = try viewModel?.register(userName: userNameTextField, passWord: passwordTextField)
            if valid! {
                let alert = UIAlertController(title: "Registration Completed", message: "", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "Done", style: .cancel, handler: nil))
                self.present(alert, animated: true, completion: nil)
                DispatchQueue.main.asyncAfter(deadline: .now() + 2.0) {
                    self.navigationController?.popViewController(animated: true)
                }
            } else  {
                let alert = UIAlertController(title: "Please Type another Name", message: "", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "Done", style: .cancel, handler: nil))
                self.present(alert, animated: true, completion: nil)
            }
        } catch { print("error")}
    }
    
    deinit {
        print ("RegisterVC Deinited")
    }
}
