//
//  RegisterViewModel.swift
//  ProfProgrPart2
//
//  Created by Sergey on 13.04.2020.
//  Copyright © 2020 Sergey. All rights reserved.
//

import Foundation
import RealmSwift

class RegisterViewModel: UIViewController {
    var model: User?
    var dbmanager: DBManager = DBManagerImpl()
    
    init(login: String, password: String) {
        self.model?.login = login
        self.model?.password = password
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func register(userName: ShakingAnimation, passWord: ShakingAnimation) throws -> Bool  {
        guard let username = userName.text, !username.isEmpty else {throw CustomError.error1(func: userName.shake())}
        guard let password = passWord.text, !password.isEmpty else {throw CustomError.error1(func: passWord.shake())}
        let user = User(login: username, password: password)
        var condition = true
        let users = dbmanager.getUser()
        var tempArray:[String] = []

        for index in users {
            tempArray.append(index.login)
        }
        if  tempArray.contains(user.login){
            print("user already exist")
            condition = false
        } else {
            dbmanager.saveUser(user: user)
            print("registration complete")
            condition = true
        }
        return condition
    }
}

enum CustomError: Error {
    case error1(func: ())
}
