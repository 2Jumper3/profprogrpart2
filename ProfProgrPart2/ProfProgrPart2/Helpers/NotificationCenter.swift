//
//  NotificationCenter.swift
//  ProfProgrPart2
//
//  Created by Sergey on 16.05.2020.
//  Copyright © 2020 Sergey. All rights reserved.
//

import Foundation
import UserNotifications

class Notifications {
    
    let notifications = UNUserNotificationCenter.current()
    let content = UNMutableNotificationContent()
    
    func requestAutoriz() {
        notifications.requestAuthorization(options: [.alert, .badge, .sound]) { (state, error) in
            if state {
                print("Notifications ON")
                
            } else {
                print("Notifications OFF")
            }
        }
    }
    
    func notificationStatus() {
        
        notifications.getNotificationSettings { (settings) in
            switch settings.authorizationStatus {
            case    .authorized:
                print("autorizated")
            case    .denied:
                print("denied")
            case    .notDetermined:
                print("notDetermined")
            case    .provisional:
                print("provisional")
            }
        }
    }

    func makeNotification() -> UNMutableNotificationContent {
        let content  = UNMutableNotificationContent()
        content.title = "Location Still Updating"
        content.subtitle = "Open App and press «Stop»"
        return content
    }
    func timeIntervalNotifTrigger() -> UNTimeIntervalNotificationTrigger {
        
        return UNTimeIntervalNotificationTrigger(timeInterval: 15, repeats: false)
    }
    
    func sendNotifRequest(content: UNMutableNotificationContent, timeInterval: UNTimeIntervalNotificationTrigger) {
        let request  = UNNotificationRequest(identifier: "backgroundNotification", content: content, trigger: timeInterval)
        let center = UNUserNotificationCenter.current()
        center.add(request) { (error) in
            if let error = error {
                print(error.localizedDescription)
            } else {
                print("Alert added!")
            }
        }
    }
    
    func removeNotifications() {
        notifications.removeAllPendingNotificationRequests()
    }
}
