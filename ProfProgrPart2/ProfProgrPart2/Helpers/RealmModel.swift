//
//  RealmModel.swift
//  ProfProgrPart2
//
//  Created by Sergey on 08.04.2020.
//  Copyright © 2020 Sergey. All rights reserved.
//

import Foundation
import RealmSwift
import CoreLocation

class NewRealmModel: Object {
    @objc dynamic var latitude = 0.0
    @objc dynamic var longitude = 0.0
    @objc dynamic var timePerSec = 0
    
    override class func primaryKey() -> String? {
        return #keyPath(timePerSec)
    }
    convenience init(lat: Double, lng: Double, timePerSec: Int) {
        self.init()
        self.latitude = lat
        self.longitude = lng
        self.timePerSec = timePerSec
    }
}
 //MARK: - Model for Register and Login VC
@objcMembers
class User: Object {
        dynamic var login = ""
        dynamic var password = ""
    
    override class func primaryKey() -> String? {
        return #keyPath(login)
    }
    
    convenience init(login: String, password: String) {
        self.init()
        self.login = login
        self.password = password
    }
}

