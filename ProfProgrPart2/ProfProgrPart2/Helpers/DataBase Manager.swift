//
//  DataBase Manager.swift
//  ProfProgrPart2
//
//  Created by Sergey on 25.05.2020.
//  Copyright © 2020 Sergey. All rights reserved.
//

import Foundation
import RealmSwift

    //MARK: - DataBase manager

protocol DBManager {
    func save(coordinate: List<NewRealmModel>)
    func getCoordinates() -> [NewRealmModel]
    func getUser() -> [User]
    func saveUser(user: User)
}

class DBManagerImpl: DBManager {
    func saveUser(user: User) {
        let realm = try! Realm()
        try! realm.write {
            realm.add(user)
        }
    }
    
    func getUser() -> [User] {
        let realm = try! Realm()
        let user = realm.objects(User.self)
        return Array(user)
    }
    
    func save(coordinate: List<NewRealmModel>) {
        let realm = try! Realm()
        try! realm.write {
            realm.delete(realm.objects(NewRealmModel.self))
            realm.add(coordinate)
        }
    }
    func getCoordinates() -> [NewRealmModel] {
        let realm = try! Realm()
        let coordinates = realm.objects(NewRealmModel.self)
        return Array(coordinates)
    }
}
