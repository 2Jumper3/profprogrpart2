//
//  LocationManager.swift
//  ProfProgrPart2
//
//  Created by Sergey on 04.05.2020.
//  Copyright © 2020 Sergey. All rights reserved.
//

import Foundation
import RxSwift
import CoreLocation

class LocationManager: NSObject  {
        static let instance = LocationManager()
        var isActive = false
        
        override init() {
            super.init()
            configureLocationManager()
        }
        
        let locationManager = CLLocationManager()
        
        let location: Variable<CLLocation?> = Variable(nil)
        
        func startUpdatingLocation() {
            locationManager.startUpdatingLocation()
            self.isActive = true
        }
        
        func stopUpdatingLocation() {
            locationManager.stopUpdatingLocation()
            self.isActive = false
        }
        
        private func configureLocationManager() {
            locationManager.delegate = self
            locationManager.allowsBackgroundLocationUpdates = true
            locationManager.pausesLocationUpdatesAutomatically = false
            locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
            locationManager.startMonitoringSignificantLocationChanges()
            locationManager.requestAlwaysAuthorization()
        }
        
    }

    extension LocationManager: CLLocationManagerDelegate {
        func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
            
            self.location.value = locations.last
        }
        
        func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
            print(error)
        }
    }
