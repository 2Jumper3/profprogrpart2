//
//  Protocols.swift
//  ProfProgrPart2
//
//  Created by Sergey on 13.04.2020.
//  Copyright © 2020 Sergey. All rights reserved.
//

import Foundation
import UIKit

class ShakingAnimation: UITextField {
    
    
    func shake()  {
        
        let animation = CABasicAnimation(keyPath: "position")
            animation.duration = 0.05
            animation.repeatCount = 5
            animation.autoreverses = true
            animation.fromValue = NSValue(cgPoint: CGPoint(x: self.center.x - 4, y: self.center.y))
            animation.toValue = NSValue(cgPoint: CGPoint(x: self.center.x + 4, y: self.center.y))
            self.layer.add(animation, forKey: "position")
    }
}

extension UIButton {
    func setBackgroundColor(color: UIColor, forState: UIControl.State) {
        self.clipsToBounds = true  // add this to maintain corner radius
        UIGraphicsBeginImageContext(CGSize(width: 1, height: 1))
        if let context = UIGraphicsGetCurrentContext() {
            context.setFillColor(color.cgColor)
            context.fill(CGRect(x: 0, y: 0, width: 1, height: 1))
            let colorImage = UIGraphicsGetImageFromCurrentImageContext()
            UIGraphicsEndImageContext()
            self.setBackgroundImage(colorImage, for: forState)
        }
    }
}
