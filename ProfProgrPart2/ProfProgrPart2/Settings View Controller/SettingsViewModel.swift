//
//  SettingsViewModel.swift
//  ProfProgrPart2
//
//  Created by Sergey on 21.05.2020.
//  Copyright © 2020 Sergey. All rights reserved.
//

import Foundation
import UIKit

protocol SettingViewModel {
    var image: UIImage {get}
    func loadImageFromDiskWith(fileName: String) -> UIImage?
    func saveImage(image: UIImage?)
}

class SettingViewModelImpl: SettingViewModel {
    
    var image = UIImage()
    
    private let model: SettingsModel?
    
    init(model: SettingsModel) {
        self.model = model
    }
    
    //MARK: - Image Load/Save Methods
   func loadImageFromDiskWith(fileName: String) -> UIImage? {

     let documentDirectory = FileManager.SearchPathDirectory.documentDirectory

       let userDomainMask = FileManager.SearchPathDomainMask.userDomainMask
       let paths = NSSearchPathForDirectoriesInDomains(documentDirectory, userDomainMask, true)

       if let dirPath = paths.first {
           let imageUrl = URL(fileURLWithPath: dirPath).appendingPathComponent(fileName)
           let image = UIImage(contentsOfFile: imageUrl.path)
           return image

       }

       return nil
    }
    
    func saveImage(image: UIImage?) {
        guard let documentsDirectory = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first else { return }
        //            let fileName = String(Int(Date().timeIntervalSince1970)) + "profProgrPart2.png"
                    let fileName = "imageName"
                    let fileURL = documentsDirectory.appendingPathComponent(fileName)
                    guard let data = image?.jpegData(compressionQuality: 1) else { return }

                    if FileManager.default.fileExists(atPath: fileURL.path) {
                        do {
                            try FileManager.default.removeItem(atPath: fileURL.path)
                            print("Removed old image")
                        } catch let removeError {
                            print("couldn't remove file at path", removeError)
                        }

                    }

                    do {
                        try data.write(to: fileURL)
                        print("file url is \(fileURL)")
                    } catch let error {
                        print("error saving file with error", error)
                    }
    }
    
    
}
