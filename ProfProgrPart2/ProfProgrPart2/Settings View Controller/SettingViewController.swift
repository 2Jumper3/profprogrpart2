//
//  SettingViewController.swift
//  ProfProgrPart2
//
//  Created by Sergey on 19.05.2020.
//  Copyright © 2020 Sergey. All rights reserved.
//

import Foundation
import UIKit
import AssetsLibrary
import AVFoundation

class SettingViewController: UIViewController {
    
    
    var imagePickerController = UIImagePickerController()
    var viewModel: SettingViewModel? {
        didSet {
            self.avatarImage.image = viewModel?.loadImageFromDiskWith(fileName: "imageName")
        }
    }
    override func viewDidLoad() {
        setupLayout()
        viewModel = SettingViewModelImpl(model: SettingsModel())
        self.navigationController?.navigationBar.isHidden = false
    }
    
    
    //MARK: - View Settings
    private(set) lazy var  avatarImage: UIImageView =  {
        let image = UIImageView()
        image.clipsToBounds = true
        image.translatesAutoresizingMaskIntoConstraints = false
        image.backgroundColor = .white
        return image
    }()

    private(set) lazy var  fromLibraryBtn: UIButton =  {
        let btn = UIButton()
        btn.translatesAutoresizingMaskIntoConstraints = false
        btn.setTitle("Choose From Library", for: .normal)
        btn.tintColor = .black
        btn.setBackgroundColor(color: .gray, forState: .disabled)
        btn.addTarget(self, action: #selector(btnLibrary), for: .touchUpInside)
        btn.backgroundColor = .darkGray
        return btn
    }()
    private(set) lazy var  fromCameraBtn: UIButton =  {
        let btn = UIButton()
        btn.translatesAutoresizingMaskIntoConstraints = false
        btn.setTitle("Take A Photo", for: .normal)
        btn.tintColor = .black
        btn.setBackgroundColor(color: .gray, forState: .disabled)
        btn.addTarget(self, action: #selector(btnCamera), for: .touchUpInside)
        btn.backgroundColor = .darkGray
        return btn
    }()
    
    private(set) lazy var  saveBtn: UIButton =  {
        let btn = UIButton()
        btn.translatesAutoresizingMaskIntoConstraints = false
        btn.setTitle("Save", for: .normal)
        btn.tintColor = .black
        btn.setBackgroundColor(color: .gray, forState: .disabled)
        btn.addTarget(self, action: #selector(btnSave), for: .touchUpInside)
        btn.backgroundColor = .darkGray
        return btn
    }()
    
    
    @objc func btnLibrary() {
        guard UIImagePickerController.isSourceTypeAvailable(.photoLibrary) else {
            return print ("No Access to Library")
        }
        let imagePicker = UIImagePickerController()
        imagePicker.sourceType = .photoLibrary
        imagePicker.allowsEditing = true
        imagePicker.delegate = self
        present(imagePicker, animated: true)
        
    }
    
    @objc func btnCamera() {
        guard UIImagePickerController.isSourceTypeAvailable(.camera) else {
            return print ("No Access to Camera")
        }
        let imagePicker = UIImagePickerController()
        imagePicker.sourceType = .camera
        imagePicker.allowsEditing = true
        imagePicker.delegate = self
        present(imagePicker, animated: true)
    }
    
    
       @objc func btnSave() {
        viewModel?.saveImage(image: avatarImage.image)
    }

    
    //MARK: - Setup Layout
    
    private func setupLayout() {
        self.view.addSubview(avatarImage)
        self.view.addSubview(fromLibraryBtn)
        self.view.addSubview(fromCameraBtn)
        self.view.addSubview(saveBtn)
        self.view.backgroundColor = .lightGray
        
        let topAnchorContraint: CGFloat = 150
        let widthAnchorContraint: CGFloat = 200
        let spacingContraint: CGFloat = 15
        let heightAnchorContraint: CGFloat = 30
        self.avatarImage.layer.cornerRadius = 100
           
           
           NSLayoutConstraint.activate([
            self.avatarImage.topAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.topAnchor, constant: topAnchorContraint),
            self.avatarImage.widthAnchor.constraint(equalToConstant: widthAnchorContraint),
            self.avatarImage.centerXAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.centerXAnchor),
            self.avatarImage.heightAnchor.constraint(equalToConstant: widthAnchorContraint),
            
            self.fromLibraryBtn.topAnchor.constraint(equalTo: self.avatarImage.bottomAnchor, constant: spacingContraint),
            self.fromLibraryBtn.widthAnchor.constraint(equalToConstant: widthAnchorContraint),
            self.fromLibraryBtn.centerXAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.centerXAnchor),
            self.fromLibraryBtn.heightAnchor.constraint(equalToConstant: heightAnchorContraint),
               
            self.fromCameraBtn.topAnchor.constraint(equalTo: self.fromLibraryBtn.bottomAnchor, constant: spacingContraint),
            self.fromCameraBtn.widthAnchor.constraint(equalToConstant: widthAnchorContraint),
            self.fromCameraBtn.centerXAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.centerXAnchor),
            self.fromCameraBtn.heightAnchor.constraint(equalToConstant: heightAnchorContraint),
            
            self.saveBtn.topAnchor.constraint(equalTo: self.fromCameraBtn.bottomAnchor, constant: spacingContraint),
            self.saveBtn.widthAnchor.constraint(equalToConstant: widthAnchorContraint),
            self.saveBtn.centerXAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.centerXAnchor),
            self.saveBtn.heightAnchor.constraint(equalToConstant: heightAnchorContraint)
           ])
       }
    
}

extension SettingViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let image = info[UIImagePickerController.InfoKey.editedImage] as? UIImage {
            avatarImage.image = image
        }
        avatarImage.clipsToBounds = true
        picker.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
}
